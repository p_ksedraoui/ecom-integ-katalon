import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner
import com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())


RunConfiguration.setExecutionSettingFile('C:\\Users\\P_KSED~1\\AppData\\Local\\Temp\\Katalon\\Test Cases\\Achat\\20181120_145400\\execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runTestCaseRawScript(
'''import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('https://www.int-gl.com/')

not_run: WebUI.click(findTestObject('Object Repository/Page_Accueil/a_ (1)'))

WebUI.click(findTestObject('Object Repository/Page_Accueil/a_Fermer_search-button'))

WebUI.setText(findTestObject('Object Repository/Page_Accueil/input_Fermer_search'), 'polo')

WebUI.click(findTestObject('Object Repository/Page_Accueil/a_Polo Ralph Lauren'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Polo slim fit Polo Ralph Laure/select_Taille'), '70002878', 
    true)

WebUI.click(findTestObject('Object Repository/Page_Polo slim fit Polo Ralph Laure/a_Ajouter au panier'))

WebUI.click(findTestObject('Object Repository/Page_Polo slim fit Polo Ralph Laure/a_Passer la commande'))

WebUI.click(findTestObject('Object Repository/Page_Votre commande  Galeries Lafay/a_COMMANDER'))

WebUI.setText(findTestObject('Page_Identification  Galeries Lafay/input_DJ INSCRIT _username'), 'ksi2@yopmail.com')

WebUI.setEncryptedText(findTestObject('Page_Identification  Galeries Lafay/input_Vous devez renseigner un'), 'F87jBibooDn7ckj4dy9PAw==')

WebUI.click(findTestObject('Object Repository/Page_Livraison  Galeries Lafayette/div_Offert_checkmark'))

WebUI.click(findTestObject('Object Repository/Page_Livraison  Galeries Lafayette/a_confirmer et passer au paiem'))

WebUI.click(findTestObject('Object Repository/Page_Paiement  Galeries Lafayette/div_Cds Cscs_checkmark'))

WebUI.click(findTestObject('Object Repository/Page_Paiement  Galeries Lafayette/label_En cochant cette case vo'))

WebUI.click(findTestObject('Object Repository/Page_Paiement  Galeries Lafayette/a_VALIDER LE PAIEMENT'))

WebUI.setText(findTestObject('Object Repository/Page_Paiement  Galeries Lafayette/input_Code de vrification de l'), '123')

WebUI.click(findTestObject('Object Repository/Page_Paiement  Galeries Lafayette/input_Un  indique les champs o'))

WebUI.click(findTestObject('Object Repository/Page_Merci  Galeries Lafayette/img_Fermer_logo-lafayette-top-'))

WebUI.closeBrowser()

''', 'Test Cases/Achat', new TestCaseBinding('Test Cases/Achat',[:]), FailureHandling.STOP_ON_FAILURE , false)
    
