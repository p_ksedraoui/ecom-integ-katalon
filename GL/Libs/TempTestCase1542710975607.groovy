import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner
import com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())


RunConfiguration.setExecutionSettingFile('C:\\Users\\P_KSED~1\\AppData\\Local\\Temp\\Katalon\\Test Cases\\authent\\20181120_114935\\execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runTestCaseRawScript(
'''import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('https://www.int-gl.com/')

not_run: WebUI.click(findTestObject('Object Repository/Page_Accueil/div_Abonnez-vous  la newslette'))

not_run: WebUI.click(findTestObject('Object Repository/Page_Accueil/a_'))

WebUI.click(findTestObject('Object Repository/Page_Accueil/img_Fermer_logo-lafayette-top-'))

WebUI.click(findTestObject('Object Repository/Page_Accueil/a_Fermer_left-off-canvas-toggl'))

WebUI.click(findTestObject('Object Repository/Page_Accueil/span_COMPTE'))

WebUI.setText(findTestObject('Object Repository/Page_Identification  Galeries Lafay/input_DJ INSCRIT _username'), 'ksi2@yopmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Identification  Galeries Lafay/input_Vous devez renseigner un'), 
    'F87jBibooDn7ckj4dy9PAw==')

WebUI.click(findTestObject('Object Repository/Page_Mon Compte  Galeries Lafayette/span_APERU DE MON COMPTE'))

WebUI.click(findTestObject('Object Repository/Page_Mon Compte  Galeries Lafayette/a_MES DONNES PERSONNELLES'))

WebUI.click(findTestObject('Object Repository/Page_Mon Compte  Galeries Lafayette/span_MES ACHATS ET RETOURS'))

WebUI.click(findTestObject('Object Repository/Page_Mon Compte  Galeries Lafayette/span_MES GALERIES PROGRAMME DE'))

WebUI.closeBrowser()

''', 'Test Cases/authent', new TestCaseBinding('Test Cases/authent',[:]), FailureHandling.STOP_ON_FAILURE , false)
    
