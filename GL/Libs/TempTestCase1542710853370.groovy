import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner
import com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())


RunConfiguration.setExecutionSettingFile('C:\\Users\\P_KSED~1\\AppData\\Local\\Temp\\Katalon\\20181120_114733\\execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runWSVerificationScript(new TestCaseBinding('',[:]), 'import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI\nimport com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile\nimport com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW\nimport com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS\nimport static com.kms.katalon.core.testobject.ObjectRepository.findTestObject\nimport static com.kms.katalon.core.testdata.TestDataFactory.findTestData\nimport static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase\nimport static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint\nimport com.kms.katalon.core.model.FailureHandling as FailureHandling\nimport com.kms.katalon.core.testcase.TestCase as TestCase\nimport com.kms.katalon.core.testdata.TestData as TestData\nimport com.kms.katalon.core.testobject.TestObject as TestObject\nimport com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint\nimport internal.GlobalVariable as GlobalVariable\nimport org.openqa.selenium.Keys as Keys\n\nWebUI.openBrowser(\'\')\n\nWebUI.navigateToUrl(\'https://www.int-gl.com/\')\n\nWebUI.click(findTestObject(\'Page_Accueil/div_Abonnez-vous  la newslette\'))\n\nWebUI.click(findTestObject(\'Page_Accueil/a_\'))\n\nWebUI.click(findTestObject(\'Page_Accueil/img_Fermer_logo-lafayette-top-\'))\n\nWebUI.click(findTestObject(\'Page_Accueil/a_Fermer_left-off-canvas-toggl\'))\n\nWebUI.click(findTestObject(\'Page_Accueil/span_COMPTE\'))\n\nWebUI.setText(findTestObject(\'Page_Identification  Galeries Lafay/input_DJ INSCRIT _username\'), \'ksi2@yopmail.com\')\n\nWebUI.setEncryptedText(findTestObject(\'Page_Identification  Galeries Lafay/input_Vous devez renseigner un\'), \'F87jBibooDn7ckj4dy9PAw==\')\n\nWebUI.click(findTestObject(\'Page_Mon Compte  Galeries Lafayette/span_APERU DE MON COMPTE\'))\n\nWebUI.click(findTestObject(\'Page_Mon Compte  Galeries Lafayette/a_MES DONNES PERSONNELLES\'))\n\nWebUI.click(findTestObject(\'Page_Mon Compte  Galeries Lafayette/span_MES ACHATS ET RETOURS\'))\n\nWebUI.click(findTestObject(\'Page_Mon Compte  Galeries Lafayette/span_MES GALERIES PROGRAMME DE\'))\n\nWebUI.closeBrowser()\n\n', FailureHandling.STOP_ON_FAILURE, true)

