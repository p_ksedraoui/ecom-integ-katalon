<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dd_Description</name>
   <tag></tag>
   <elementGuidId>6e5eae68-d3f8-4df5-b2c9-e54d1eab30c8</elementGuidId>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>dd</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>js-accordion-navigation</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>accordion-navigation js-accordion-navigation active</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    
        Description
    
    
    
        Sac pour tablette ou lecteur électronique. Il est muni d'une bandoulière pour un porté épaule ou travers. Poche matelassée pour protéger votre appareil. Fermeture zippée rehaussée d'un rabat à bride sangle boucle clip. Poche arrière à glissière. DimensionsHauteur : 34 cmLargeur : 28 cmProfondeur : 3 cm

        
        

            
            

            
            

            
                
            

            
            

            
            

            
            

            
            

            
            

            
            

            
            

            
            

            
            

            
            
                Référence fournisseur : 323892
            

            
            

            
            

        

        
        
            Composition
            100% polyester. 
        

        
        

        
        

        
        


    


</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;js-accordion-navigation&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <value>//dd[@id='js-accordion-navigation']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='product-description-anchor']/div/dl/dd</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Voir plus'])[1]/following::dd[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Conseils'])[1]/preceding::dd[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//dd</value>
   </webElementXpaths>
</WebElementEntity>
