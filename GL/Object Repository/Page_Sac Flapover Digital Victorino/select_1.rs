<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_1</name>
   <tag></tag>
   <elementGuidId>45769043-7fca-4310-a63e-3058df9d1f0c</elementGuidId>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>select select-quantity js-select-quantity js-select-data-ga</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                    
                        1
                        
                    
                        
                        2
                    
                        
                        3
                    
                        
                        4
                    
                        
                        5
                    
                        
                        6
                    
                        
                        7
                    
                        
                        8
                    
                        
                        9
                    
                        
                        10
                    
                
                
            </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;description&quot;)/div[@class=&quot;row block-toolbox&quot;]/div[@class=&quot;columns small-24 xmedium-11 xmedium-offset-1 large-11 large-offset-1 block-select-colors-and-size&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;block-select-size select-size-list columns&quot;]/div[@class=&quot;columns small-5 medium-3 small-offset-1 block-quantity&quot;]/select[@class=&quot;select select-quantity js-select-quantity js-select-data-ga&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='description']/div/div[7]/div[2]/div/div/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Couleur : Noir'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Victorinox Travel Gear'])[3]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ajouter au panier'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//select</value>
   </webElementXpaths>
</WebElementEntity>
