<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_VALIDER LE PAIEMENT</name>
   <tag></tag>
   <elementGuidId>969da6ce-0768-4d36-aa81-8c09012d02ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='payment-page-info']/div[5]/div/div/div[2]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;payment-page-info&quot;)/div[@class=&quot;payment-confirm-block-wrapper row&quot;]/div[@class=&quot;payment-confirm-block&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;small-24  medium-10 columns confirm&quot;]/a[@class=&quot;button black js-pay-button&quot;][count(. | //a[@href = '#' and @class = 'button black js-pay-button' and @data-ga = 'gaEvent|gaEvent_action:Paiement|gaEvent_category:Commande|gaEvent_label:bouton_confirmer' and (text() = '
                    
                    VALIDER LE PAIEMENT
                ' or . = '
                    
                    VALIDER LE PAIEMENT
                ')]) = count(//a[@href = '#' and @class = 'button black js-pay-button' and @data-ga = 'gaEvent|gaEvent_action:Paiement|gaEvent_category:Commande|gaEvent_label:bouton_confirmer' and (text() = '
                    
                    VALIDER LE PAIEMENT
                ' or . = '
                    
                    VALIDER LE PAIEMENT
                ')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button black js-pay-button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ga</name>
      <type>Main</type>
      <value>gaEvent|gaEvent_action:Paiement|gaEvent_category:Commande|gaEvent_label:bouton_confirmer</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                    VALIDER LE PAIEMENT
                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;payment-page-info&quot;)/div[@class=&quot;payment-confirm-block-wrapper row&quot;]/div[@class=&quot;payment-confirm-block&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;small-24  medium-10 columns confirm&quot;]/a[@class=&quot;button black js-pay-button&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='payment-page-info']/div[6]/div/div/div[2]/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Conditions Générales de Vente'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Une erreur est survenue. Veuillez nous excuser de la gêne occasionnée'])[3]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Conditions générales du programme de fidélité Mes Galeries'])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='galerieslafayette.com'])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <value>(//a[contains(@href, '#')])[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[6]/div/div/div[2]/a</value>
   </webElementXpaths>
</WebElementEntity>
