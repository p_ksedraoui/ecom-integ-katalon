<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_En cochant cette case vo</name>
   <tag></tag>
   <elementGuidId>980b3ca3-2c79-41b4-a23d-6ea5f373ebb2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='payment-page-info']/div[5]/div/div/div/label</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//label[@for = 'paymentAcceptRules-null' and (text() = '
                    En cochant cette case, vous acceptez et reconnaissez avoir pris connaissance des
                    Conditions Générales de Vente
                    Galerieslafayette.com et de ses Offres Partenaires.
                ' or . = '
                    En cochant cette case, vous acceptez et reconnaissez avoir pris connaissance des
                    Conditions Générales de Vente
                    Galerieslafayette.com et de ses Offres Partenaires.
                ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>paymentAcceptRules-null</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    En cochant cette case, vous acceptez et reconnaissez avoir pris connaissance des
                    Conditions Générales de Vente
                    Galerieslafayette.com et de ses Offres Partenaires.
                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;payment-page-info&quot;)/div[@class=&quot;payment-confirm-block-wrapper row&quot;]/div[@class=&quot;payment-confirm-block&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;small-24 medium-14 columns checkboxRules&quot;]/label[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='payment-page-info']/div[5]/div/div/div/label</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MASQUER'])[1]/following::label[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='VOIR'])[1]/following::label[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Conditions générales du programme de fidélité Mes Galeries'])[1]/preceding::label[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//label</value>
   </webElementXpaths>
</WebElementEntity>
