<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Vous venez dajouter  votre</name>
   <tag></tag>
   <elementGuidId>a84cf41a-4f15-4be2-9eaf-f89664036482</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='popinAddToCart']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-content popin-add-to-cart</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

        

        

            Vous venez d'ajouter à votre panier

            

                

                

    
        
            
                
                
                
                
            
        
    



                
                    

    
        
            
            
                Polo slim fit
            
            
            
                Polo Ralph Lauren
            
        
    
    
        Couleur : Vert
        Taille : M
        Quantité : 1
    


                    

    
        
            

    
    
        
            85,00 €
        
    
    
        
    

            


    
    

    
    

    
    



        
    



                    
                        Passer la commande
                        Continuer votre shopping
                    
                
            

            
            
                






    




            

        
    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;popinAddToCart&quot;)/div[@class=&quot;modal-content popin-add-to-cart&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='popinAddToCart']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fermer'])[3]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Catalogues interactifs'])[1]/following::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[4]/div</value>
   </webElementXpaths>
</WebElementEntity>
