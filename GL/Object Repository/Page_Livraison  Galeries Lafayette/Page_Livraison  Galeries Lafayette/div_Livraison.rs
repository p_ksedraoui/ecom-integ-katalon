<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Livraison</name>
   <tag></tag>
   <elementGuidId>7113a56f-3e2a-4ec6-a45f-e3223a76585d</elementGuidId>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>js-overlay-block overlay-block</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
        

    

   Livraison

    

        

        

        
            
                

    
        
            
                
                
                
                    

    
    

        

        
            
                
                    
                
            
        

        
            
                Retrait En Magasin
            
            Mercredi 28 novembre

            

        

        
            
                
                    
                    Offert
                
            
            
        

        

        

        
            
                
                    
                
            
        

        
            
                Retrait En Magasin
            

            
                Mercredi 28 novembre
            

            
                
                    
                    Offert
                
            

            

        

        

    


                
            
        
            
                
                    
                        
                            

    
    

        

        
            
                
                    
                
            
        

        
            
                Livraison à Domicile
            
            Entre le 28 et le 30 novembre

            

        

        
            
                
                    4,90 €
                    
                
            
            
        

        

        

        
            
                
                    
                
            
        

        
            
                Livraison à Domicile
            

            
                Entre le 28 et le 30 novembre
            

            
                
                    4,90 €
                    
                
            

            

        

        

    


                        
                        
                    
                
                
                
            
        
            
                
                    
                        
                        
                            

    
    

        

        
            
                
                    
                
            
        

        
            
                Livraison à Domicile Express
            
            Mardi 27 novembre avant 13h

            

        

        
            
                
                    9,90 €
                    
                
            
            
        

        

        

        
            
                
                    
                
            
        

        
            
                Livraison à Domicile Express
            

            
                Mardi 27 novembre avant 13h
            

            
                
                    9,90 €
                    
                
            

            

        

        

    


                        
                    
                
                
                
            
        
            
                
                
                    
                        
                            

    
    

        

        
            
                
                    
                
            
        

        
            
                Retrait en Point Relais Express
            
            Mardi 27 novembre

            

        

        
            
                
                    3,90 €
                    
                
            
            
        

        

        

        
            
                
                    
                
            
        

        
            
                Retrait en Point Relais Express
            

            
                Mardi 27 novembre
            

            
                
                    3,90 €
                    
                
            

            

        

        

    


                        
                        
                    
                
                
            
        
    


            
        

        

    
        
            Livraison à l’international
        
    



    



        
        
            
                
                
            
        
    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths desktop landscape&quot;]/body[1]/div[@class=&quot;page checkout-page delivery-page&quot;]/div[@class=&quot;main-nav off-canvas-wrap&quot;]/div[@class=&quot;inner-wrap&quot;]/div[@class=&quot;checkout-bg&quot;]/div[@class=&quot;js-overlay-block overlay-block&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PAIEMENT'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='LIVRAISON'])[1]/following::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div/div/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
