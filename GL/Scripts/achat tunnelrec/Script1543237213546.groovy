import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://tunnel-rec.int-gl.com/')

WS.delay(5)

WebUI.click(findTestObject('Object Repository/Page_Accueil/a_Fermer_search-button (1)'))

WebUI.setText(findTestObject('Object Repository/Page_Accueil/input_Fermer_search (1)'), 'VICTORINOX TRAVEL GEAR')

WebUI.navigateToUrl('https://tunnel-rec.int-gl.com/p/sac+flapover+digital-victorinox+travel+gear/35795814/320')

WS.delay(2)

WebUI.scrollToPosition(0, 800)

WS.delay(2)

WebUI.click(findTestObject('Page_Sac Flapover Digital Victorino/div__reveal-modal-bg'))

WS.delay(2)

WebUI.click(findTestObject('Page_Sac Flapover Digital Victorino/a_Passer la commande'))

WS.delay(2)

WebUI.click(findTestObject('Page_Votre commande  Galeries Lafay/a_COMMANDER'))

WebUI.setText(findTestObject('Object Repository/Page_Identification  Galeries Lafay/input_DJ INSCRIT _username (1)'), 'ksi2@yopmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Identification  Galeries Lafay/input_Vous devez renseigner un (1)'), 
    'F87jBibooDn7ckj4dy9PAw==')

WebUI.click(findTestObject('Object Repository/Page_Identification  Galeries Lafay/a_Poursuivre ma commande'))

WebUI.focus(findTestObject('Page_Livraison  Galeries Lafayette/Page_Livraison  Galeries Lafayette/div_Livraison  Domicile'))

WebUI.click(findTestObject('Page_Livraison  Galeries Lafayette/Page_Livraison  Galeries Lafayette/div_Livraison  Domicile'))

WS.delay(2)

WebUI.focus(findTestObject('Page_Livraison  Galeries Lafayette/a_confirmer et passer au paiem'))

WS.delay(2)

WebUI.click(findTestObject('Page_Livraison  Galeries Lafayette/a_confirmer et passer au paiem'))

WebUI.click(findTestObject('Page_Paiement  Galeries Lafayette/div_Cds Cscs_checkmark'))

WebUI.clickOffset(findTestObject('Page_Paiement  Galeries Lafayette/label_En cochant cette case vo'), 0, 0)

WebUI.click(findTestObject('Page_Paiement  Galeries Lafayette/a_VALIDER LE PAIEMENT'))

WebUI.setText(findTestObject('Page_Paiement  Galeries Lafayette/input_Code de vrification de l'), '123')

WebUI.click(findTestObject('Page_Paiement  Galeries Lafayette/input_Un  indique les champs o'))

WS.delay(2)

WebUI.closeBrowser()

