import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.int-gl.com/')

WebUI.click(findTestObject('Object Repository/Page_Accueil/div_Abonnez-vous  la newslette'))

WebUI.click(findTestObject('Page_Accueil/a_'))

WebUI.click(findTestObject('Object Repository/Page_Accueil/img_Fermer_logo-lafayette-top-'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_Accueil/a_Fermer_left-off-canvas-toggl'))

WebUI.click(findTestObject('Object Repository/Page_Accueil/span_COMPTE'))

WebUI.setText(findTestObject('Object Repository/Page_Identification  Galeries Lafay/input_DJ INSCRIT _username'), 'ksi2@yopmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Identification  Galeries Lafay/input_Vous devez renseigner un'), 
    'F87jBibooDn7ckj4dy9PAw==')

WebUI.click(findTestObject('Page_Identification  Galeries Lafay/a_Me connecter'))

WS.delay(10)

WebUI.click(findTestObject('Object Repository/Page_Mon Compte  Galeries Lafayette/span_APERU DE MON COMPTE'))

WS.delay(10)

WebUI.click(findTestObject('Object Repository/Page_Mon Compte  Galeries Lafayette/a_MES DONNES PERSONNELLES'))

WS.delay(10)

WebUI.click(findTestObject('Object Repository/Page_Mon Compte  Galeries Lafayette/span_MES ACHATS ET RETOURS'))

WS.delay(10)

WebUI.click(findTestObject('Object Repository/Page_Mon Compte  Galeries Lafayette/span_MES GALERIES PROGRAMME DE'))

WebUI.closeBrowser()

